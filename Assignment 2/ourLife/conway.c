#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define HEIGHT 25
#define WIDTH 25
#define ALIVE 1
#define DEAD 0

// make the rest of the function calls easier to read
typedef int Grid[HEIGHT][WIDTH];

void printTable(Grid table) {
	int height, width;

	for (height = 0; height < HEIGHT; height++) {
		for (width = 0; width < WIDTH; width++) {
			if (table[height][width] == ALIVE) {
				printf("*");
			} else {
				printf("-");
			}
		}
		printf("\n");
	}
	printf("\n");
}


// you already have a printTable, no need for this
// clear was a better name
void clearTable(Grid table) {
	int height, width;
	for (height = 0; height < HEIGHT; height++) {
		for (width = 0; width < WIDTH; width++) {
			table[height][width] = DEAD;
		}
	}
}

/*
void askUser(Grid tableA) {
	int i;
	int n;
	int height, width;

	printf("Enter the amount of initial organisms: ");
	scanf("%d", &n);
	for (i = 0; i < n; i++) {
		printf("Enter dimensions (x y) where organism %d will live: ", i + 1);
		scanf("%d %d", &height, &width);
		tableA[height][width] = ALIVE;
	}
	
	printTable(tableA);
	printf("Generation 0");
}
*/

int getNeighborValue(Grid table, int row, int col) {
	if (row < 0 || row >= HEIGHT || col < 0
		 || col >= WIDTH || table[row][col] != ALIVE) return 0;
	
	else return 1;
}


int getNeighborCount(Grid table, int row, int col) {
	int neighbor = 0;

	neighbor += getNeighborValue(table, row - 1, col - 1);
	neighbor += getNeighborValue(table, row - 1, col);
	neighbor += getNeighborValue(table, row - 1, col + 1);
	neighbor += getNeighborValue(table, row, col - 1);
	neighbor += getNeighborValue(table, row, col + 1);
	neighbor += getNeighborValue(table, row + 1, col - 1);
	neighbor += getNeighborValue(table, row + 1, col);
	neighbor += getNeighborValue(table, row + 1, col + 1);
	
	return neighbor;
}

void calculate(Grid tableA) {
	Grid tableB;
	int neighbor, height, width;

	for (height = 0; height < HEIGHT; height++) {
		for (width = 0; width < WIDTH; width++) {
			neighbor = getNeighborCount(tableA, height, width);
			// change this arund to remove the ? : notation
			if (neighbor==3) {
				tableB[height][width] = ALIVE;
			} else if (neighbor == 2 && tableA[height][width] == ALIVE) {
				tableB[height][width] = ALIVE;
			} else {
				tableB[height][width] = DEAD;
			}
		}
	}
	// used to be swap
	for (height = 0; height < HEIGHT; height++) {
		for (width = 0; width < WIDTH; width++) {
			tableA[height][width] = tableB[height][width];
		}
	}
}

// user entry is a pain for testing
// here's some code to load test data
void loadTestData(Grid table) {
	/*// toggle
	table[3][4] = ALIVE;
	table[3][5] = ALIVE;
	table[3][6] = ALIVE;
	
	// glider
	table[10][4] = ALIVE;
	table[10][5] = ALIVE;
	table[10][6] = ALIVE;
	table[11][6] = ALIVE;
	table[12][5] = ALIVE;
	*/
	srand(time(NULL));
	int x = 0, y = 0;
	for(y = 0; y < HEIGHT; y++){
		for(x = 0; x < WIDTH; x++){
			table[x][y] = rand() & ALIVE;
		}
	}
}

int main(void) {
	Grid table;
	char end;
	int generation = 0;

	clearTable(table);
	// askUser(table);
	loadTestData(table);
	printTable(table);

	while(end != 'q') {
		printf("Generation %d\n", ++generation);
		printf("Press q to quit or 1 to continue: ");
		scanf(" %c", &end);		
		calculate(table);
		printTable(table);
	}
	return 0;
}

