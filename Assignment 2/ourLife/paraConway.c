#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

#define ITERATIONS 10
#define HEIGHT 10
#define WIDTH 10
#define ALIVE 1
#define DEAD 0

// make the rest of the function calls easier to read
typedef int Grid[HEIGHT][WIDTH];
typedef int Column[HEIGHT];

void printTable(Grid table) {
	int height, width;

	for (height = 0; height < HEIGHT; height++) {
		for (width = 0; width < WIDTH; width++) {
			if (table[height][width] == ALIVE) {
				printf("*");
			} else {
				printf("-");
			}
		}
		printf("\n");
	}
	printf("\n");
}


// you already have a printTable, no need for this
// clear was a better name
void clearTable(Grid table) {
	int height, width;
	for (height = 0; height < HEIGHT; height++) {
		for (width = 0; width < WIDTH; width++) {
			table[height][width] = DEAD;
		}
	}
}

/*
void askUser(Grid tableA) {
	int i;
	int n;
	int height, width;

	printf("Enter the amount of initial organisms: ");
	scanf("%d", &n);
	for (i = 0; i < n; i++) {
		printf("Enter dimensions (x y) where organism %d will live: ", i + 1);
		scanf("%d %d", &height, &width);
		tableA[height][width] = ALIVE;
	}
	
	printTable(tableA);
	printf("Generation 0");
}
*/

int getNeighborValue(Grid table, int row, int col) {
	if (row < 0 || row >= HEIGHT || col < 0
		 || col >= WIDTH || table[row][col] != ALIVE) return 0;
	
	else return 1;
}


int getNeighborCount(Grid table, int row, int col) {
	int neighbor = 0;

	neighbor += getNeighborValue(table, row - 1, col - 1);
	neighbor += getNeighborValue(table, row - 1, col);
	neighbor += getNeighborValue(table, row - 1, col + 1);
	neighbor += getNeighborValue(table, row, col - 1);
	neighbor += getNeighborValue(table, row, col + 1);
	neighbor += getNeighborValue(table, row + 1, col - 1);
	neighbor += getNeighborValue(table, row + 1, col);
	neighbor += getNeighborValue(table, row + 1, col + 1);
	
	return neighbor;
}

void calculate(Grid tableA, int start, int end) {
	Grid tableB;

	int neighbor, height, width;

	for (height = 0; height < HEIGHT; height++) {
		for (width = start; width < end; width++) {
			neighbor = getNeighborCount(tableA, height, width);

			if (neighbor==3) {
				tableB[height][width] = ALIVE;
			} else if (neighbor == 2 && tableA[height][width] == ALIVE) {
				tableB[height][width] = ALIVE;
			} else {
				tableB[height][width] = DEAD;
			}
		}
	}

	for (height = 0; height < HEIGHT; height++) {
		for (width = start; width < end; width++) {
			tableA[height][width] = tableB[height][width];
		}
	}
}

// user entry is a pain for testing
// here's some code to load test data
void loadTestData(Grid table) {
	/*// toggle
	table[3][4] = ALIVE;
	table[3][5] = ALIVE;
	table[3][6] = ALIVE;
	
	// glider
	table[10][4] = ALIVE;
	table[10][5] = ALIVE;
	table[10][6] = ALIVE;
	table[11][6] = ALIVE;
	table[12][5] = ALIVE;
	*/
	srand(time(NULL));
	int x = 0, y = 0;
	for(y = 0; y < HEIGHT; y++){
		for(x = 0; x < WIDTH; x++){
			table[x][y] = rand() & ALIVE;
		}
	}
}

int min(int a, int b){
	return ( a < b ) ?a:b;
}

void para_range(int n1, int n2, int *nprocs, int *irank, int *istart, int *iend) {
	int iwork1;
	int iwork2;
	iwork1 = ( n2 - n1 + 1 ) / *nprocs;
	iwork2 = ( ( n2 - n1 + 1 ) % *nprocs);
	*istart = *irank * iwork1 + n1 + min(*irank, iwork2);
	*iend = *istart + iwork1 - 1;
	if ( iwork2 > *irank ) 
		*iend  = *iend + 1;
}

int main(int argc, char** argv) {

	//Initialize start and end points to calculate over
	int i, width, height, istart, iend, count = 0;
	//Initialize the rank number and number of processes in MPI_COMM_WORLD
	int iproc, nproc;
	//Instantiate column that will be sent and received
	Column mesgColumn;
	Grid table, tableA, tableB;
	//char end;
	//int generation = 0;

	clearTable(table);
	// askUser(table);
	loadTestData(table);
	printTable(table);
	for(height = 0; height < HEIGHT; height++){
		for(width = 0; width < WIDTH; width++){
			tableA[height][width] = tableB[height][width] = table[height][width];
		}
	}
	MPI_Status status;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
	printf("%d", iproc);
	//para_range(0,WIDTH,&nproc,&iproc,&istart,&iend);

	//If current rank is the process managing left half of grid
	if(iproc == 0){

		//while(end != 'q') {
		while(count < ITERATIONS) {
			count++;
			if(count != 0){
				MPI_Recv(mesgColumn, HEIGHT, MPI_INT, iproc+1, 123,
				 MPI_COMM_WORLD, &status);
				for(i = 0; i<HEIGHT; i++){
					tableA[i][WIDTH/2] = mesgColumn[i];
				}
			}
			//printf("Generation %d\n", ++generation);
			//printf("Press q to quit or 1 to continue: ");
			//scanf(" %c", &end);		
			calculate(tableA, 0, WIDTH/2);
			for(i = 0; i < HEIGHT; i++){
				mesgColumn[i] = tableA[i][WIDTH/2-1];
			}

		  	MPI_Send(mesgColumn, HEIGHT, MPI_INT, iproc+1, 123,
		  	 MPI_COMM_WORLD);
			//printTable(table);
			//printf("\n");
		}
	}

	//If current rank is the process managing the right half of the grid
	if( iproc == 1 ){
		//while(end != 'q') {
		while(count < ITERATIONS){
			count++;
			//printf("Generation %d\n", ++generation);
			//printf("Press q to quit or 1 to continue: ");
			//scanf(" %c", &end);
			if(count != 0){
				MPI_Recv(mesgColumn, HEIGHT, MPI_INT, iproc-1, 123,
				 MPI_COMM_WORLD, &status);

				for(i = 0; i<HEIGHT; i++){
					tableB[i][WIDTH/2-1] = mesgColumn[i];
				}
			}
			printf("mesgColumn is:\n");
			for(i = 0; i < HEIGHT; i++){
				printf("%d", mesgColumn[i]);
			}
			calculate(tableB, WIDTH/2, WIDTH);
			for(i = 0; i < HEIGHT; i++){
				mesgColumn[i] = tableB[i][WIDTH/2];
			}


		  	MPI_Send(mesgColumn, HEIGHT, MPI_INT, iproc-1, 123,
		  	 MPI_COMM_WORLD);
			//printTable(table);
			//printf("\n");
		}
	}
	MPI_Finalize();
	return 0;
}

