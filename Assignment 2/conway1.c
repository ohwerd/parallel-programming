#include <stdio.h>
#include <stdlib.h>

#define NR_NEIGHBOURS 8
#define WIDTH_MAP 3
#define HEIGHT_MAP 3

typedef struct cell_t_ {
    struct cell_t_ *neighbours[NR_NEIGHBOURS];
    int on;
    int prevOn;
} T_CELL;
 
typedef struct world_t_ {
    T_CELL **array;
    int width;
    int height;
    void *mem;
} world_t;
 
void drawMap(world_t *world, FILE *pOutput)
{
    int x, y;
 
    for(y = 0; y < world->height; y++) {
        for(x = 0; x < world->width; x++) {
            fprintf(pOutput, "%c", (world->array[y][x]).on ? '1' : ' ');
        }
        fputc((int)'\n', pOutput);
    }
    fflush(pOutput);
}
 
void randomizeMap(world_t *world)
{
    int x, y;
 	srand(time(NULL));
    for(y = 0; y < world->height; y++) {
        for(x = 0; x < world->width; x++) {
            (world->array[y][x]).on = world->array[y][x].prevOn = rand() & 1;
        }
    }
}
 
//Altering map state according to previous resulting state 
void updateMap(world_t *world)
{
    int x, y, i, neighbours;

    //Checking for neighbours in the previous world's cells
    for(y = 0; y < world->height; y++) {
        for(x = 0; x < world->width; x++, neighbours = 0) {
            for(i = 0; i < NR_NEIGHBOURS; i++)
                if((world->array[y][x].neighbours[i]) && ((world->array[y][x]).neighbours[i]->on == 1))
                    neighbours++;
 
            if((neighbours < 2) || (neighbours > 3 )) {
                (world->array[y][x]).on = 0;
            }
            if(((neighbours == 3) || (neighbours == 2)) && (world->array[y][x].prevOn == 1)){
            	world->array[y][x].on = 1;
            }
            else if(neighbours == 3){
                (world->array[y][x]).on = 1;
            }
        }
    }

 	//Updating world so all new cell states are now set to previous cell states
    for(y = 0; y < world->height; y++) {
        for(x = 0; x < world->width; x++) {
            world->array[y][x].prevOn = world->array[y][x].on;
        }
    }
}
 
void destroyMap(world_t *world)
{
    free(world->mem);
}
 
int createMap(world_t *world, int width, int height)
{
    int i, j;
    unsigned long base   = sizeof(T_CELL *) * height;
    unsigned long rowlen = sizeof(T_CELL)   * width;
 
    if(!(world->mem = calloc(base + (rowlen * height), 1)))
        return 0;
 
    world->array  = world->mem;
    world->width  = width;
    world->height = height;
 
    for(i = 0; i < height; i++) {
        world->array[i] = world->mem + base + (i * rowlen);
    }

    /* The neighbours are entered like so:
       0 1 2
	   3 X 4 
       5 6 7
    */
    for(i = 0; i < height; i++) {
        for(j = 0; j < width; j++) {
            if(j != 0) {
                (world->array[i][j]).neighbours[3] = &(world->array[i][j - 1]);
            }
 
            if(i != 0) {
                (world->array[i][j]).neighbours[1] = &(world->array[i - 1][j]);
            }
 
            if(j != (width - 1)) {
                (world->array[i][j]).neighbours[4] = &(world->array[i][j + 1]);
            }
 
            if(i != (height - 1)) {
                (world->array[i][j]).neighbours[6] = &(world->array[i + 1][j]);
            }
 
            if((i != 0) && (j != 0)) {
                (world->array[i][j]).neighbours[0] = &(world->array[i - 1][j - 1]);
            }
 
            if((i != (height - 1)) && (j != (width - 1))) {
                (world->array[i][j]).neighbours[7] = &(world->array[i + 1][j + 1]);
            }
 
            if((i != (height - 1)) && (j != 0)) {
                (world->array[i][j]).neighbours[5] = &(world->array[i + 1][j - 1]);
            }
 
            if((i != 0) && (j != (width - 1))) {
                (world->array[i][j]).neighbours[2] = &(world->array[i - 1][j + 1]);
            }
        }
    }
 
    return 1;
}
 
int main(int argc, char *argv[]) {
    world_t gameoflife;
    int i = 0, j = 0, count = 0;;
 
    if(createMap(&gameoflife, WIDTH_MAP, HEIGHT_MAP)) {
        randomizeMap(&gameoflife);
        while (1){
            for(i = 0; i < gameoflife.width; i++)
                for(j = 0; j < gameoflife.height; j++)
                    if(gameoflife.array[i][j].prevOn)
                        count++;
            if(count == 0)
                break;
            printf("\n%d\n", count);
            count = 0;
            drawMap(&gameoflife, stdout);
            getchar();
            fflush(stdin);
            updateMap(&gameoflife);
            printf("neighbours of [0] are: [4] = %d, [7] = %d and [6] = %d\n", gameoflife.array[0][0].neighbours[4]->prevOn,
            	gameoflife.array[0][0].neighbours[7]->prevOn,
            	gameoflife.array[0][0].neighbours[6]->prevOn);

            
        }
        destroyMap(&gameoflife);
    }
 
    return 0;
}