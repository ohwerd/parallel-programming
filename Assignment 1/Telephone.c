/* program Telephone */

/* Adapted from mpihello.f by drs 
  Ed Crupi (edwardcrupi), Felipe Schmidt (fmschmidt)
	Asserts:
		That the argument supplied is no greater than the message
		size.

Exercise 3a

Assuming that each MPI process is executed on its own node and that
 the interconnection network has a certain node to node latency and
 a certain bandwidth between a node and the interconnection
 network. It will take (ignoring computation):

                p * n 
p  *  _alpha +  ______        seconds until the message is printed and all
                _beta         processes are aware of the message
                
    Where:  p = number of processes
            n = size of message in bytes
    _alpha    = latency per process in seconds
      _beta   = bandwidth/datarate in bytes per second    

*/

#include <mpi.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define MESSAGESIZE 10

int main(int argc, char **argv)
{
  int rank, size, i;
  char message[MESSAGESIZE];

  int argvSize = strlen(argv[1]);
  int messageSize = sizeof(message)/sizeof(message[0]);

  //Checks for invalid input
  if(argvSize>messageSize){
      printf("Please use a smaller message (less than %d characters).\n"
      , (int)sizeof(message));
      return(0);
  }


  MPI_Status status;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  //Checks for good number of processes.
  if(size < 2){
  	printf("Please run with at least two processes.\n");
  	MPI_Finalize();
  	return(-1);
  }

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  printf("Hey! My rank is %d\n", rank);

  //If first process get message from input argument and send it on
  if(rank==0){
  	for(i = 0; i < strlen(argv[1]); i++){
  		message[i] = argv[1][i];
  	}

  	MPI_Send(message,
  	strlen(message),
  	MPI_CHAR,
  	rank+1,
  	123,
  	MPI_COMM_WORLD);
  
  	MPI_Finalize();
  	return 0;
  }

  //And if last process receive message from previous process and
  // print it then exit
  else if(rank==size-1){
  	printf("The message is: ");

  	  	MPI_Recv(message, 10, MPI_CHAR, rank-1, 123, MPI_COMM_WORLD,
  	 &status);

  	for(i=0; i<10; i++)
  	{
  		printf("%c", message[i]);
  	}
  	printf("\n");
  	fflush(stdout);
  	MPI_Finalize();
  	return 0;
  }

  //But if anything else just receive message from previous process
  // and send it to next process
  else {
  	MPI_Recv(message, 10, MPI_CHAR, rank-1, 123, MPI_COMM_WORLD,
  	 &status);

  MPI_Send(message,
	 strlen(message),
	 MPI_CHAR,
	 rank+1,
	 123,
	 MPI_COMM_WORLD);

  }

  MPI_Finalize();
  return 0;
  
}