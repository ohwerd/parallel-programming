//Broadcast.c
//Edward Crupi

//Broadcasts a message using MPI.

#include <mpi.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define MESSAGESIZE 10

int main(int argc, char **argv)
{
  int rank, size, i;
  char message[MESSAGESIZE];

  int argvSize = strlen(argv[1]);
  int messageSize = sizeof(message)/sizeof(message[0]);

  //Checks for invalid input
  if(argvSize>=messageSize){
      printf("Please use a smaller message (less than %d characters).\n"
      , (int)sizeof(message));
      return(0);
  }

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  //Checks for good number of processes.
  if(size < 2){
  	printf("Please run with at least two processes.\n");
  	MPI_Finalize();
  	return(-1);
  }

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  printf("Hey! My rank is %d\n", rank);

  //If first process get message from input argument and send it on
  if(rank==0){
  	for(i = 0; i < strlen(argv[1]); i++){
  		message[i] = argv[1][i];
  	}
  }

  MPI_Bcast(message,
  	size,
  	MPI_CHAR,
  	0,
  	MPI_COMM_WORLD);
  
  if(rank == size-1){
  		printf("After being broadcast, message is: %s\n", message);
  }

  MPI_Finalize();
  return 0;
  
}

